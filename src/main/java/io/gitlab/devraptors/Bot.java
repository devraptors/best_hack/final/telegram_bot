package io.gitlab.devraptors;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;



public  class Bot extends TelegramLongPollingBot {
    String[] tools = null;
    String[] gets = {"/finance", "/dots", "/reports"};
    private static final String TOKEN = "1630519826:AAGyY0vFwijK8APbUZBqcffA8FmrIA6Uq2o";
    private static final String USERNAME = "BestHackTestBot";

    public static JSONObject parseJsonFromRequest(InputStream stream) throws IOException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject)jsonParser.parse(new InputStreamReader(stream, "UTF-8"));
        } catch (ParseException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    private String sendGet(String req) throws Exception {
        HttpGet request = new HttpGet(req);
        CloseableHttpClient httpCli = HttpClients.createDefault();
        try (CloseableHttpResponse response = httpCli.execute(request);) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                return result;
            }
        }
        return null;
    }
    private JSONObject sendJsonFinance(String req, JSONObject jsona, long userId) throws Exception {
        URL obj = new URL(req);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);
        JSONObject json = new JSONObject();
        json = jsona;
        json.put("chat_id", userId);
        OutputStream os = con.getOutputStream();
        os.write(json.toString().getBytes("UTF-8"));
        os.close();

        InputStream request = con.getInputStream();
        JSONObject data = Bot.parseJsonFromRequest(request);
        System.out.println(data);
        return data;
    }
    private void sendJsonTools(String req, JSONObject jsona, long userId) throws Exception {
        URL obj = new URL(req);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);
        JSONObject json = new JSONObject();
        json = jsona;
        json.put("chat_id", userId);
        OutputStream os = con.getOutputStream();
        os.write(json.toString().getBytes("UTF-8"));
        os.close();
    }

    public Bot(DefaultBotOptions options){
        super(options);
    }
    public String getBotToken(){
        return TOKEN;
    }
    public String getBotUsername(){
        return USERNAME;
    }
    private ReplyKeyboardMarkup createKeyboard(String[] list)
    {

        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        for (String button: list){
            KeyboardRow row = new KeyboardRow();
            row.add(button);
            keyboard.add(row);
        }

        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }
    public void onUpdateReceived(Update updates){
        Message message = updates.getMessage();
        Database database = Database.getInstance();

        if(message != null && message.hasText()){
            String text = message.getText();
            long userId = message.getChatId();

            User currentUser = database.getUserById(userId);

            try {
                switch(text){
                    case "/start": {
                        SendMessage sendMessage = new SendMessage(userId, "здарова, отец, нажми кнопку tools, чтобы ознаакомиться с товаром!");
                        String[] mess = {"/tools"};
                        sendMessage.setReplyMarkup(createKeyboard(mess));
                        execute(sendMessage);
                        break;
                    }
                    case "/tools": {
                        currentUser.setState(State.TOOLS);
                        JSONParser jsonParser = new JSONParser();
                        JSONObject jsonObject = (JSONObject)jsonParser.parse(sendGet("http://localhost:5000/api/tools"));
                        JSONArray array = (JSONArray) jsonObject.get("tools");
                        tools = new String[array.size()+1];
                        for(int i=0;i<array.size();i++) {
                            tools[i] = (String) array.get(i);
                        }
                        SendMessage sendMessage = new SendMessage(userId, "отец, биржа предлагает торговать:");
                        //заполнение тулсов с Артема
                        tools[tools.length-1] = "/trades";
                        sendMessage.setReplyMarkup(createKeyboard(tools));
                        execute(sendMessage);
                        break;
                    }
                    case "/trades":{
                        currentUser.setState(State.STARTTIME);

                        SendMessage sendMessage = new SendMessage(userId, "отец, введи время начала торгов в формате: ЧЧ::ММ::СС");
                        execute(sendMessage);
                        break;
                    }
                    case "/finance": {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("chat_id", userId);
                        JSONObject inputObj = sendJsonFinance("http://localhost:5000/api/finance", jsonObject, userId);
                        ArrayList<JSONObject> tools = (ArrayList<JSONObject>) inputObj.get("tools");
                        String result = "";
                        for(JSONObject value: tools) {
                            String name = value.get("tool").toString();
                            String amount = value.get("amount").toString();
                            String balance = value.get("balance").toString();
                            result = result + "инструмент: " + name + " количество: " + amount + " балланс: " + balance + "\n";
                        }
                        SendMessage sendMessage = new SendMessage(userId, result);
                        execute(sendMessage);
                        break;
                    }
                    case "/dots":{
                        SendMessage sendMessage = new SendMessage(userId, "отец, кода пока нет");
                        execute(sendMessage);
                        break;
                    }
                    case "/reports":{
                        currentUser.setState(State.TAKETOOL);
                        SendMessage sendMessage = new SendMessage(userId, "отец, укажи инструмент, по каторому хочешьполучить развернутый отчет в формате csv");
                        execute(sendMessage);
                        break;
                    }
                    default:
                        switch(currentUser.getState()){
                            case TOOLS: {
                                boolean isTool = false;
                                for(int i = 0; i < 9; i++) {
                                    if (text.equals(tools[i])) {
                                        isTool = true;
                                        break;
                                    }
                                }
                                if(isTool) {
                                    if(currentUser.setTool(text)){
                                        SendMessage sendMessage = new SendMessage(userId, "инструмент успешно добавлен");
                                        execute(sendMessage);
                                    }
                                    else {
                                        SendMessage sendMessage = new SendMessage(userId, "инструмент успешно удален");
                                        execute(sendMessage);
                                    }
                                }
                                else{
                                    SendMessage sendMessage = new SendMessage(userId, "ЧТО ЭТО ЗА СЛОВО, ГОВОРИ НОРМАЛЬНО, РУССКИЙ ЧЕЛОВЕК!");
                                    execute(sendMessage);
                                }
                                break;
                            }
                            case STARTTIME:{
                                if (currentUser.setStartTime(text)){
                                    currentUser.setState(State.FINISHTIME);
                                    SendMessage sendMessage = new SendMessage(userId, "Время старта принято! Введите время окончания в формате ЧЧ::ММ::СС");

                                    execute(sendMessage);
                                }
                                else {
                                    SendMessage sendMessage = new SendMessage(userId, "ЧТО ЭТО ЗА ВРЕМЯ, ГОВОРИ НОРМАЛЬНО, РУССКИЙ ЧЕЛОВЕК!");
                                    execute(sendMessage);
                                }
                                break;
                            }
                            case FINISHTIME:{
                                if (currentUser.setStartTime(text)){
                                    currentUser.setState(State.END);
                                    SendMessage sendMessage = new SendMessage(userId, "Время окончания принято!");
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("chat_id", userId);

                                    JSONArray data = new JSONArray();
                                    for (int i = 0; i < currentUser.numOfTool; i++){
                                        Object toolObj = (currentUser.tools[i]);
                                        data.add(toolObj);
                                    }
                                    jsonObject.put("tools", data);
                                    jsonObject.put("start", currentUser.startTime);
                                    jsonObject.put("end", currentUser.finishTime);
                                    sendJsonTools("http://localhost:5000/api/trade", jsonObject, userId);
                                    sendMessage.setReplyMarkup(createKeyboard(gets));
                                    execute(sendMessage);
                                }
                                else {
                                    SendMessage sendMessage = new SendMessage(userId, "ЧТО ЭТО ЗА ВРЕМЯ, ГОВОРИ НОРМАЛЬНО, РУССКИЙ ЧЕЛОВЕК!");
                                    execute(sendMessage);
                                    //api finans
                                    //api dots
                                    //api reports
                                }
                                break;
                            }
                            case TAKETOOL:{
                                boolean isTool = false;
                                for(int i = 0; i < 9; i++) {
                                    if (text.equals(tools[i])) {
                                        isTool = true;
                                        break;
                                    }
                                }
                                if(isTool){
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("chat_id", userId);
                                    jsonObject.put("tool", text);
                                    JSONObject inputObj = sendJsonFinance("http://localhost:5000/api/finance", jsonObject, userId);
                                    ArrayList<JSONObject> tools = (ArrayList<JSONObject>) inputObj.get("tools");
                                    String result = "";
                                    for(JSONObject value: tools) {
                                        String timestamp = value.get("timestamp").toString();
                                        String direction = value.get("direction").toString();
                                        String price = value.get("price").toString();
                                        String amount = value.get("amount").toString();
                                        String volume = value.get("volume").toString();
                                        String balance = value.get("balance").toString();
                                        result = result + timestamp + ";" + direction + ";" + price + ";" + amount + ";"+ volume + ";" + balance + "\n";
                                        String fileName = text + ".csv";
                                        File file = new File("/home/app/target" + fileName);
                                        if (file.createNewFile()){
                                            FileWriter writer = new FileWriter(file);
                                            writer.write(result);
                                            SendDocument sendDocument = new SendDocument();
                                            sendDocument.setChatId(userId);
                                            sendDocument.setNewDocument(file);
                                            sendDocument(sendDocument);
                                            SendMessage sendMessage = new SendMessage(userId, "формат файла: timestamp;direction;price;amount;volume;balance");
                                            execute(sendMessage);
                                        }
                                        else{
                                            System.out.println("File already exists.");
                                        }
                                    }
                                }
                            }


                        }
                        break;
                }

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
