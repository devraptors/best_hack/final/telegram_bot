package io.gitlab.devraptors;

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.DefaultBotOptions;

import java.util.*;

enum State {
    START,
    TOOLS,
    STARTTIME,
    FINISHTIME,
    END,
    TAKETOOL
}


public class User {
    long userId;
    String fromCity;
    String[] tools;
    State state;
    String startTime, finishTime;
    int numOfTool;
    int numOfTime;
    public User(long userId){
        this.userId = userId;
        this.tools = new String[10];

        this.numOfTime = 0;
        this.numOfTool = 0;
    }
    public boolean setFinishTime(String time){
        String REGEX = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
        if (time.matches(REGEX)) {
            finishTime = time;
            return true;
        }
        return false;
    }
    public boolean setStartTime(String time){
        String REGEX = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
        if (time.matches(REGEX)) {
            startTime = time;
            return true;
        }
        return false;
    }


    public void dellTool(int numOfDellitedTool) {
        for(int i = numOfDellitedTool; i < 9 - 1; i++){
            this.tools[i] = this.tools[i + 1];
        }
        this.numOfTool--;
    }

    public boolean setTool(String tool) {
        for(int i = 0; i < this.numOfTool; i++) {
            if (this.tools[i].equals(tool)){
                dellTool(i);
                return false;
            }
        }
        this.tools[this.numOfTool] = tool;
        this.numOfTool++;
        return true;
    }

    public void setState(State state) {
        this.state = state;
    }

    public long getUserId() {
        return userId;
    }


    public String[] getTools(){ return this.tools;}
    public String getStarTime(){ return this.startTime;}
    public String getFinishTime(){ return this.finishTime;}
    public State getState() {
        return state;
    }
}
